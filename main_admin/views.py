from babel.dates import format_date

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from django.db.models import Q

from events.models import Event
from users.models import User


class MainPage(TemplateView):
    """Представление для начальной страницы"""
    template_name = 'main/main.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        return context


class UserAdminView(LoginRequiredMixin, TemplateView):
    """Представление для админки"""
    template_name = 'main/user_admin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['user'] = self.request.user
        context['events'] = Event.objects.all()
        context['my_events'] = Event.objects.filter(Q(creator=user) | Q(participants=user)).distinct()

        return context


def refresh_page(request):
    """Обновляет страницу каждые 30 секунд (без перезагрузки страницы)"""
    all_events = Event.objects.all()
    events_data = []
    for event in all_events:
        events_data.append({'id': event.id, 'name': event.name})

    event_id = request.GET.get('event_id')
    participants_data = []
    if event_id:
        event = get_object_or_404(Event, pk=event_id)
        participants = event.participants.all()
        for participant in participants:
            participants_data.append({'id': participant.id, 'name': f"{participant.first_name} {participant.last_name}"})
    return JsonResponse({'events_data': events_data, 'participants_data': participants_data})


def get_event_data(request):
    """Получает данные выбранного события"""
    event_id = request.GET.get('event_id')
    event = get_object_or_404(Event, pk=event_id)
    event_date = event.date
    formatted_date = format_date(event_date, format='long', locale='ru_RU')
    event_data = {
        'id': event.id,
        'name': str(event.name),
        'description': str(event.description),
        'date': str(formatted_date)
    }
    participants_data = []
    participants = event.participants.all()
    for participant in participants:
        participants_data.append({'id': participant.id, 'name': f"{participant.first_name} {participant.last_name}"})
    if event.creator == request.user or event.participants.filter(id=request.user.id).exists():
        is_mine = True
    else:
        is_mine = False

    return JsonResponse({'event_data': event_data, 'participants_data': participants_data, 'is_mine': is_mine})


def manage_participant_actions(request):
    """Добавляет или удаляет участника"""
    event_id = request.GET.get('event_id')
    action = request.GET.get('action')
    event = get_object_or_404(Event, pk=event_id)
    user = request.user
    participant_data = [
        {
            'id': user.id,
            'name': f"{user.first_name} {user.last_name}"
         }
    ]
    errors = False
    if action == 'add':
        event.participants.add(user)
        result = 'Вы успешно присоединились к событию!'

    else:
        participants_count = event.participants.count()
        # Так как по логике БД все поля должны быть NOT NULL, если участник единственный, удаление его
        # из события приведет к ошибке
        if participants_count == 1:
            result = 'Вы являетесь единственным участником, поэтому Вас нельзя удалить из списка!'
            errors = True
        else:
            event.participants.remove(user)
            result = 'Вы успешно отказались от участия в событии!'
    event.save()

    return JsonResponse(data={'result': result, 'participant_data': participant_data, 'errors': errors}, status=200)


def get_participant_data(request):
    """Получает все данные выбранного участника"""
    person_id = request.GET.get('person_id')
    person = get_object_or_404(User, pk=person_id)
    data_joined = person.data_joined
    formatted_data_joined = format_date(data_joined, format='long', locale='ru_RU')
    date_of_birth = person.date_of_birth
    formatted_date_of_birth = format_date(date_of_birth, format='long', locale='ru_RU')

    person_data = {
        'username': person.username,
        'name': f'{person.first_name} {person.last_name}',
        'data_joined': str(formatted_data_joined),
        'date_of_birth': str(formatted_date_of_birth),
    }

    return JsonResponse(person_data)


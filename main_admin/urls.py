from django.urls import path

from rest_framework import routers

from .views import get_event_data, get_participant_data, manage_participant_actions, refresh_page

router = routers.DefaultRouter()

urlpatterns = [
    path('get_event_data/', get_event_data, name='get_event_data'),
    path('get_participant_data/', get_participant_data, name='get_participant_data'),
    path('manage_participant_actions/', manage_participant_actions, name='manage_participant_actions'),
    path('refresh_page/', refresh_page, name='refresh_page')
]
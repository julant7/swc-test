# Generated by Django 3.2.4 on 2023-09-27 17:46

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_alter_user_date_joined'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='data_joined',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата регистрации'),
        ),
    ]

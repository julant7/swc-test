import datetime

from django.contrib.auth import authenticate

from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from swc_test import settings

from .models import User


class UserListingField(serializers.RelatedField):

    def to_internal_value(self, value):
        return value

    def to_representation(self, value):
        return value.id


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super(CustomTokenObtainPairSerializer, self).validate(attrs)
        data.update({'errors': None})
        data.update({'id': self.user.id})
        data.update({'first_name': self.user.first_name})
        data.update({'last_name': self.user.last_name})
        expriration = (datetime.datetime.now() + datetime.timedelta(days=31)).timestamp()
        data['expires_in'] = expriration
        return data


class LoginSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.CharField(
        label='Логин',
        write_only=True
    )
    password = serializers.CharField(
        label='Пароль',
        style={'input_type': 'password'},
        trim_whitespace=True,
        write_only=True
    )

    class Meta:
        model = User
        fields = ['username', 'password']

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            if not User.objects.filter(username=username).exists():
                raise serializers.ValidationError({'error': 'Пользователя с таким логином не существует!'},
                                                  code='authorization')

            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            if not user:
                raise serializers.ValidationError({'error': 'Неверный пароль!'}, code='authorization')

        else:
            raise serializers.ValidationError({'error': 'Оба поля "логин" и "пароль" являются обязательными!'}, code='authorization')

        attrs['user'] = user
        return attrs


class UserRegisterSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.CharField(
        style={'placeholder': 'Введите логин (юзернейм)'},
        label='Логин*',
        required=True
    )
    first_name = serializers.CharField(
        style={'placeholder': 'Введите имя'},
        label='Имя*',
        required=True
    )
    last_name = serializers.CharField(
        style={'placeholder': 'Введите фамилию'},
        label='Фамилия*',
        required=True
    )
    password = serializers.CharField(
        style={'placeholder': 'Введите пароль', 'input_type': 'password'},
        label='Пароль*',

        required=True
    )
    password2 = serializers.CharField(
        style={'placeholder': 'Введите пароль еще раз', 'input_type': 'password'},
        label='Пароль*',
        required=True
    )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'password', 'password2', 'date_of_birth']

    def save(self, *args, **kwargs):
        user = User(username=self.validated_data['username'])
        password = self.validated_data['password']
        password2 = self.validated_data['password2']
        if password != password2:
            raise serializers.ValidationError({'error': "Пароли не совпадают!"})
        user.set_password(password)
        user.date_joined = datetime.datetime.now()
        user.save()
        return user

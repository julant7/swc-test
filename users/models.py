from datetime import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils import timezone


class MyUserManager(BaseUserManager):
    def _create_user(self, email, username, password, **kwargs):
        if not email:
            raise ValueError("Вы не ввели адрес электронной почты")
        if not username:
            raise ValueError("Вы не ввели логин")
        user = self.model(
            email=self.normalize_email(email),
            username=username,
            **kwargs
        )
        user.set_password(password)
        user.date_joined = datetime.now()
        user.save(using=self._db)
        return user

    def create_user(self, email, username, password):
        return self._create_user(email, username, password)

    def create_superuser(self, email, username, password):
        user = self._create_user(email, username, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    email = models.EmailField(max_length=255, verbose_name='Адрес электронной почты')
    username = models.CharField(max_length=55, unique=True, verbose_name='Логин')
    first_name = models.CharField(max_length=255, verbose_name='Имя')
    last_name = models.CharField(max_length=255, verbose_name='Фамилия')
    data_joined = models.DateTimeField(verbose_name='Дата регистрации', default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_of_birth = models.DateField(verbose_name='Дата рождения', null=True, blank=True)

    objects = MyUserManager()

    def __str__(self):
        return self.username

from datetime import datetime

from django.contrib.auth import login
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import User
from .serializers import UserRegisterSerializer, LoginSerializer, CustomTokenObtainPairSerializer
from .forms import UserRegistrationForm
from . import serializers


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


@api_view(['GET'])
def users_overview(request):
    """Все методы RESTful API для пользователей"""
    api_urls = {
        'Запрос токена для аутентификации': '/token',
        'Обновление токена': '/token/refresh/',
        'Проверка токена': '/token/verify/',
        'Зарегистрироваться': '/drf_register',
    }

    return Response(api_urls)


class LoginView(APIView):
    """Аутентификация пользователя с помощью DRF"""
    permission_classes = permissions.AllowAny,
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = serializers.LoginSerializer(data=self.request.data, context={'request': self.request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)

        user_data = {
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name
        }

        return Response({'errors': None, 'result': user_data}, status=status.HTTP_202_ACCEPTED)


class RegisterUserView(CreateAPIView):
    """Регистрация пользователя с помощью DRF"""
    queryset = User.objects.all()
    serializer_class = UserRegisterSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = UserRegisterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data={'errors': None, 'result': serializer.data}, status=status.HTTP_200_OK)
        else:
            errors = serializer.errors
            return Response(data={'errors': errors})


def register(request):
    """Регистрация пользователя (после регистрации выполняется вход)"""
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])
            new_user.date_joined = datetime.now()
            new_user.save()
            login(request, new_user)
            return HttpResponseRedirect(reverse('user_admin'))

    else:
        form = UserRegistrationForm()
    return render(request, 'registration/register.html', {'form': form})

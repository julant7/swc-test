from django.urls import path, include

from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

from .views import RegisterUserView, LoginView, register, users_overview, CustomTokenObtainPairView

router = routers.DefaultRouter()

urlpatterns = [
    path('', users_overview, name='users_home'),
    path('token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('drf_login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('drf_login/', LoginView.as_view(), name='drf_login'),
    path('drf_register/', RegisterUserView.as_view(), name='drf_register'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('register/', register, name='register'),
]
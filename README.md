# swc-test
## Тестовое задание от «Центра программного обеспечения» на позицию Backend Developer

## Использованные технологии:

### Backend:
- Django
- MySQL

### Frontend:
- Bootstrap
- jQuery


## Запуск приложения

### Клонирование репозитория
```sh
$ git clone https://gitlab.com/julant7/swc-test.git
$ cd swc-test
```
### Создание виртуальной среды

```sh
$ virtualenv2 --no-site-packages env
$ source env/bin/activate
```
### Создание .env и ввод переменных окружения
```sh
SECRET_KEY='Ваше значение'
DEBUG='Ваше значение'
```
### Установка зависимостей
```sh
(env)$ pip install -r requirements.txt
```
### Выполнение миграций
```sh
(env)$ python manage.py migrate
```
### Запуск приложения
```sh
(env)$ python manage.py runserver
```

## Заметки
- В RESTful аутентификация представлена в виде JWT. Некоторые методы нельзя протестировать непосредственно на странице DRF, необходим токен для запроса
- Если пользователь является единственным участником, его нельзя удалить из участников события

from django.contrib import admin
from django.urls import path, include

from main_admin.views import MainPage
from main_admin.views import UserAdminView

urlpatterns = [
    path('user_admin/', UserAdminView.as_view(), name='user_admin'),
    path('admin/', admin.site.urls),
    path('events/', include('events.urls')),
    path('users/', include('users.urls')),
    path('main_admin/', include('main_admin.urls')),
    path('', MainPage.as_view(), name='start_page'),
]

from .models import Event
from rest_framework import serializers
from users.serializers import UserListingField
from users.models import User


class EventSerializer(serializers.ModelSerializer):

    participants = UserListingField(queryset=User.objects.all(), many=True)

    class Meta:
        model = Event
        fields = ('name', 'description', 'date', 'creator', 'participants',)

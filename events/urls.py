from django.urls import path

from rest_framework import routers

from .views import delete_event, event_overview, add_event, view_events, update_event, join_event,\
    leave_event

router = routers.DefaultRouter()

urlpatterns = [
    path('', event_overview, name='event_home'),
    path('create/', add_event, name='add_event'),
    path('all/', view_events, name='view_events'),
    path('update/', update_event, name='update_event'),
    path('delete/', delete_event, name='delete_event'),
    path('join_event/', join_event, name='add_participant_to_event'),
    path('leave_event/', leave_event, name='delete_participant_from_event')
]

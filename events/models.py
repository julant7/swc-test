from django.db import models
from django.conf import settings


class Event(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Заголовок')
    description = models.TextField(verbose_name='Текст')
    date = models.DateField(verbose_name='Дата создания')
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.PROTECT,
                                related_name='creator',
                                verbose_name='Создатель',
                                )
    participants = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                          related_name='participants',
                                          verbose_name='Участники')

    objects = models.Manager()

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'

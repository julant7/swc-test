from swc_test import settings
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes
from rest_framework import status
from rest_framework.response import Response

from .models import Event

from .serializers import EventSerializer
import jwt


def decode_jwt(token):
    if not token:
        return Response(
            {"error": "JWT Токен не указан"}, status=status.HTTP_400_BAD_REQUEST
        )
    try:
        decoded_payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=["HS256"]
        )
    except jwt.DecodeError:
        return Response(
            {"error": "Неверный JWT токен"}, status=status.HTTP_401_UNAUTHORIZED
        )
    except jwt.ExpiredSignatureError:
        return Response(
            {"error": "JWT токен истёк"}, status=status.HTTP_401_UNAUTHORIZED
        )
    return decoded_payload


@api_view(["GET"])
def event_overview(request):
    """Все методы RESTful API для событий"""
    api_urls = {
        "Список событий": "/all",
        "Создание события": "/create",
        "Редактирование события": "/update",
        "Удаление события создателем": "/delete",
        "Добавление в участники события": "/join_event",
        "Удаление из участников события": "/leave_event",
        "Примечание": "Для последних 3 методов нужна аутентификации через токен; "
                      "их нельзя использовать на странице DRF.",
    }

    return Response(api_urls)


@api_view(["GET"])
def view_events(request):
    """Функция для просмотра всех существующих событий или событий по заданному фильтру"""
    if request.query_params:
        events = Event.objects.filter(**request.query_params.dict())
    else:
        events = Event.objects.all()

    if events:
        serializer = EventSerializer(events, many=True)
        return Response({"errors": None, "result": serializer.data})
    else:
        return Response(
            {"errors": "Ни одного события ещё не существует"},
            status=status.HTTP_404_NOT_FOUND,
        )


@api_view(["POST"])
def add_event(request):
    """Функция для добавления события"""

    event = EventSerializer(data=request.data)

    if event.is_valid():
        event.save()
        return Response({"error": None, "result": event.data})
    else:
        return Response({"error": event.errors}, status=status.HTTP_404_NOT_FOUND)


@api_view(["POST"])
def update_event(request):
    """Функция для редактирования события"""

    jwt_token = request.headers.get('Authorization')

    decoded_jwt = decode_jwt(jwt_token)

    if type(decoded_jwt) == Response: return decoded_jwt

    event_id = request.data.get("id")
    if event_id:
        try:
            event = Event.objects.get(pk=event_id)
        except Event.DoesNotExist:
            return Response(
                {"error": "Указан неверный id события!"},
                status=status.HTTP_404_NOT_FOUND,
            )
        data = EventSerializer(instance=event, data=request.data)
        if data.is_valid():
            user_id = decoded_jwt['user_id']
            if event.creator.id == user_id:
                data.save()
                return Response({"errors": None, "result": data.data})
            else:
                return Response(
                    {"error": "Пользователь не является автором события"}, status=status.HTTP_401_UNAUTHORIZED
                )
        else:
            return Response(
                {"error": "Событие не найдено"}, status=status.HTTP_404_NOT_FOUND
            )
    else:
        return Response(
            {"error": "Не указан идентификатор события"},
            status=status.HTTP_400_BAD_REQUEST,
        )


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
def delete_event(request):
    """Удаляет событие при условии, что пользователь - создатель события"""

    jwt_token = request.headers.get('Authorization')

    decoded_jwt = decode_jwt(jwt_token)

    if type(decoded_jwt) == Response: return decoded_jwt

    if not jwt_token:
        return Response(
            {"error": "JWT токен не был указан"}, status=status.HTTP_401_UNAUTHORIZED
        )

    event_id = request.data.get("id")
    if event_id:
        try:
            event = Event.objects.get(pk=event_id)
        except Event.DoesNotExist:
            return Response(
                {"error": "Указан неверный id события!"},
                status=status.HTTP_404_NOT_FOUND,
            )

        user_id = decoded_jwt["user_id"]
        if event.creator.id == user_id:
            event.delete()
            return Response({"error": None, "result": "Событие успешно удалено!"})
        else:
            return Response(
                data={"error": "Данный пользователь не является создателем события"}
            )

    return Response(
        {
            "error": 'Не указан идентификатор события. Укажите его с помощью {"id": "<Ваш id>"'
        },
        status=status.HTTP_400_BAD_REQUEST,
    )


@api_view(["POST"])
def join_event(request):
    """Добавляет пользователя в список участников"""

    jwt_token = request.headers.get('Authorization')

    decoded_jwt = decode_jwt(jwt_token)

    if type(decoded_jwt) == Response: return decoded_jwt

    event_id = request.data.get("id")
    if event_id:
        try:
            event = Event.objects.get(pk=event_id)
        except Event.DoesNotExist:
            return Response(
                {"error": "Указан неверный id события!"},
                status=status.HTTP_404_NOT_FOUND,
            )

        user_id = decoded_jwt["user_id"]

        if event.participants.filter(id=user_id).exists():
            return Response(
                {"error": None, "result": "Пользователь уже участвует в событии"},
                status=status.HTTP_200_OK,
            )
        else:
            event.participants.add(user_id)
            event.save()
            return Response(
                {"error": None, "result": "Пользователь был успешно добавлен в событие"},
                status=status.HTTP_200_OK,
            )

    return Response(
        {"error": 'Не указан id события. Укажите его с помощью {"id": "<Ваш id>"'},
        status=status.HTTP_400_BAD_REQUEST,
    )


@api_view(["POST"])
def leave_event(request):
    """Удаляет пользователя из списка участников"""

    jwt_token = request.headers.get('Authorization')

    decoded_jwt = decode_jwt(jwt_token)

    if type(decoded_jwt) == Response: return decoded_jwt

    event_id = request.data.get("id")
    if event_id:
        try:
            event = Event.objects.get(pk=event_id)
        except Event.DoesNotExist:
            return Response(
                {"error": "Указан неверный id события!"},
                status=status.HTTP_404_NOT_FOUND,
            )

        user_id = decoded_jwt["user_id"]
        if not event.participants.filter(id=user_id).exists():
            return Response(
                {"error": None, "result": "Пользователь не участвует в событии"},
                status=status.HTTP_200_OK,
            )
        else:
            event.participants.remove(user_id)
            event.save()
            return Response(
                {"error": None, "result": "Пользователь был успешно удален из участников события"},
                status=status.HTTP_200_OK,
            )

    return Response(
        {"error": 'Не указан id события. Укажите его с помощью {"id": "<Ваш id>"'},
        status=status.HTTP_400_BAD_REQUEST,
    )
